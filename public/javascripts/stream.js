const streamer = e => {
  console.warn("Stream error");
  console.warn(e);
}

fetch("/get-ndjson")
	.then((res) => { 
	  return can.ndjsonStream(res.body);
	 })
	.then(todosStream => { 
	  var reader = todosStream.getReader();

	  reader.read()
		  .then(read = result => {
		    if (result.done) {
		      console.log('Done.');
		      return;
		    }

		    console.log(result.value);
		    render(result.value);
		    
		    reader.read().then(read, streamer);
		  }, streamer);
	 });

let counter = 0;

render = val => {
  const div = document.createElement('div');
  div.append(`Fetched row ${++counter} - id: ${val.id}, email: ${val.email}`);
  document.getElementsByTagName('body')[0].append(div);
}