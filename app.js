const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const fs = require('fs');
const ndjson = require('ndjson');
const axios = require('axios');

const indexRouter = require('./routes/index');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'twig');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

/** 
	Read NDJSON and send to client
	------------------------------
**/
app.get('/get-ndjson', (req, res) => {
  const chunks = [];

  let readStream = fs.createReadStream(__dirname + '/public/example.ndjson').pipe(ndjson.parse());
  
  readStream.on('data', (data) => {
    chunks.push(JSON.stringify(data));
  });

  //Using setInterval() to throttle the stream so that we can see it in action.
  readStream.on('end', () => {
    let id = setInterval(() => {
      if (chunks.length) {
        res.write(chunks.shift() + '\n');
      } else {
        clearInterval(id);
        res.end();
      }
    }, 100);
  });
});


/** 
	Writes JSON response to disk as NDJSON
	--------------------------------------
**/
app.get('/create-ndjson', (req, res) => {
 	// begin the ingest process
 	const writeSteam = fs.createWriteStream(__dirname + '/public/example.ndjson', {flags:'a'});

	axios.get('https://jsonplaceholder.typicode.com/users')
		.then(res => {
	      if(res.data) {
	      	console.log('Writing json...');
	      	res.data.map((item) => {
				writeSteam.write(JSON.stringify(item) + '\n');
			});
			writeSteam.end();
	      }
	    })
		.then(() => {
			console.log('Writing done');
		})
	    .catch(error => {
	      console.log(error)
	    });

 	res.redirect('/');
});

/** 
	Fetch and parse remote ndjson
	-----------------------------
**/
app.get('/webhook', (req, res) => {
	//Fetch file and parse
	axios.get('http://localhost:8080/example.ndjson', { responseType:'stream' })
		.then(response => {
			const chunks = [];
			let stream = response.data.pipe(ndjson.parse());

			stream.on('data', (data) => {
				console.log(data);
				chunks.push(data);
			});

			stream.on('end', () => {
				console.log(`Done - Chunks: ${chunks.length}`);
			});

		  	res.json({'message': 'ok'});
		})
		.catch(err => {
			console.log(err)
			res.send(err);
		});
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
